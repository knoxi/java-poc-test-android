/**************************************************************************************
 Liar! Comms Test  Activity                                Team Knoxi
 Building IT Systems (BITS)                             +   Anthony Dera
 CPT111 SP2 2016                                        +   Gregory Dubois
                                                        +   Mark Fletcher
 (C) Team Knoxi                                         +   Darren Koski
                                                        +   Michael Noble

 CommsTestActivity.java

 This Activity is to allow connectivity tests (Android<->Knoxi Server) such as HTTP GET
 and POST for troubleshooting / debugging.
 **************************************************************************************/

package app.liarsdice.android.knoxi.xyz.liarsdice;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

// Used for JSON communications testing of Liar! and can be removed before final release - MF

public class CommsTestActivity extends AppCompatActivity {

    private TextView tvServerGetResponse; //  Used to identify the element showing response from GET
    private TextView tvServerPostResponse;//  Used to identify the element showing response from POST

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comms_test);


        final Button button_commstesthttpget = (Button) findViewById(R.id.button_commstesthttpget);
        final Button button_commstesthttppost = (Button) findViewById(R.id.button_commstesthttppost);

        button_commstesthttpget.setOnClickListener(new Button.OnClickListener(){
                    public void onClick(View v){
                        //  Launch new activity
                        button_commstesthttpget.setText(R.string.button_commstestwait);
                        new GetHTTPData().execute("http://www.knoxi.xyz/liarsdice/gameroom.php/gamesroom/235");
                        button_commstesthttpget.setText(R.string.button_commstesthttpgetresponse);
                        button_commstesthttppost.setText(R.string.button_commstesthttppostrequest);
                    }
                }
        );

        button_commstesthttppost.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Launch new activity
                button_commstesthttppost.setText(R.string.button_commstestwait);
                new PostHTTPData().execute("http://www.knoxi.xyz");
                button_commstesthttppost.setText(R.string.button_commstesthttppostresponse);
                button_commstesthttpget.setText(R.string.button_commstesthttpgetrequest);
            }
        });
    }

    public class GetHTTPData extends AsyncTask<String,  String, String>{
        @Override
        protected String doInBackground(String... params) {

        tvServerGetResponse = (TextView) findViewById(R.id.tv_ServerGetResponse);

        HttpURLConnection connection = null;            //  Initialise objects
        BufferedReader reader = null;

        try {
            URL url;
            url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();                       //  Establish connection

            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();

            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);                   // Read in from buffer
            }

            return buffer.toString();

        }  catch (IOException e) {
            e.printStackTrace();                        //  Error handling
        } finally

        {
            if (connection != null) {
                connection.disconnect();                //  Gracefully disconnect if session established
            }
            try {
                if (reader != null) {                   //  Gracefully close the datareader if established
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();                    //  Display Stack Trace in event of an error
            }
        }
        return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);                // Get the result from the execution
            tvServerGetResponse.setText(result);        // Update the TextView with the result
        }
    }

    public class PostHTTPData extends AsyncTask<String,  String, String> {
        @Override
        protected String doInBackground(String... params) {
            String result = "";
            BufferedReader reader=null;

            // Send data
            try
            {

                // Defined URL  where to send data
                URL url = new URL("http://www.knoxi.xyz/liarsdice");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( "test data" );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                result = sb.toString();
            }
            catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }

            // Show response on activity
            tvServerGetResponse.setText(result);
            return null;
        }
    }
}