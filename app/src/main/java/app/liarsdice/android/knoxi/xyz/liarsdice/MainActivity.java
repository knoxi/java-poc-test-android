/**************************************************************************************
 Liar! Main Activity                                Team Knoxi
 Building IT Systems (BITS)                             +   Anthony Dera
 CPT111 SP2 2016                                        +   Gregory Dubois
                                                        +   Mark Fletcher
 (C) Team Knoxi                                         +   Darren Koski
                                                        +   Michael Noble

 MainActivity.java

 This is the main screen for the application.
 **************************************************************************************/

package app.liarsdice.android.knoxi.xyz.liarsdice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;                                           //  Used for event handling - MF
import android.widget.Button;                                       //  Used for buttons - MF


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Used for JSON communications testing of Liar! and can be removed before final release - MF
        final Button button_commstestactivity = (Button) findViewById(R.id.button_commstest);

        button_commstestactivity.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v){
                        //  Launch new activity
                        Intent intent = new Intent(getApplicationContext(), CommsTestActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }
}
